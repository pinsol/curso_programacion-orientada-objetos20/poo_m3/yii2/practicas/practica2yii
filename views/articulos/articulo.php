<?php

use yii\widgets\DetailView;


echo DetailView::widget([
    'model' => $modelo,
    /*'labels'=> ['texto_largo' -> 'noticia'], */
    'attributes' => [
        'titulo',
        'texto_largo',
    ],
]);
