<?php
    use yii\helpers\Html;
?>

  <div  class="col-sm-5 col-md-5">
    <div class="thumbnail">
        <h3><?= $model->titulo ?></h3>
        <p><?= $model->texto_largo ?></p>
        <div class="caption altura1"> <center> <?= \yii\helpers\Html::img("@web/image/" . $model->foto,[
            'alt'=>"alternativo",
            ]) ?> </center>
      </div>
    </div>
  </div>
