<?php
use yii\widgets\ListView;

/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>
<div  class="row">

    <?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_articulos',
    
]);
?>
</div>
