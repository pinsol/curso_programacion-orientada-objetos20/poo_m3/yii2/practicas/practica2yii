-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-03-2020 a las 09:56:37
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `practica2yii2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `texto_corto` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `texto_largo` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL,
  `foto` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id`, `titulo`, `texto_corto`, `texto_largo`, `foto`) VALUES
(1, 'articulo1', 'Un producto que tomas siempre puede dar cancer... solo puede, ¿eh?', 'Este producto, que siempre lo hemos tomado como no cancerigeno; es mas se creia preventor de ello, segun un estudio de una universidad que no sabemos ni situar en el mapa, dicen que puede producir cancer. Realizando un monton de pruebas que no se sabe en que situaciones se realizan ha llegado a ese resultado', 'foto2.jpg'),
(2, 'articulo2', 'Fotografo que realiza fotos desgarradoras pero se consideran aun asi preciosas y merecedoras de recibir un premio que ni conocias', 'El fotografo de algun pais nordico ha realizado una serie de fotografias en un pais con condiciones desfavorables. Captura todo tipo de situaciones extremas, crudas y desgarradoras a las que presenta para ser premiadas y gana el premio por considerarse bellas, creyendo que de esta forma va a conseguir algo en ese pais. Ojala así fuera', 'foto2.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `texto` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `foto` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `texto`, `foto`) VALUES
(1, 'titular1', 'Noticia impactante en tu barrio que ha pasado a alguien muy cercano pero nunca te habias percatado. Alguien que parecia normal y majo. Ahora tu barrio es trending topic... ¡aprovecha! Que mas da ya tu vecino.', 'foto1.jpg'),
(2, 'titular2', 'Noticia escalofriante en un lugar remoto del que no puedes hacer nada por evitarlo. Hace sentir impotencia pero a la vez te hace pensar la suerte que tienes de estar donde estas. Miras a tu alrededor y piensas en disfrutar de lo que tienes.', 'foto1.jpg'),
(3, 'titular3', 'Entrevista de uno de los personajes publicos mas reconocidos actualmente del que no sabras nada en el futuro. Seguramente acabe haciendo imagenes subidas de tono de alguna manera ...¿o ya lo ha hecho? ¡Vamos, buscalo!', 'foto1.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
