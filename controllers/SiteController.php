<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\models\Articulos;
use app\models\Noticias;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $articulos = new ActiveDataProvider([
            'query' => Articulos::find(),
        ]);
        
         $noticias = new ActiveDataProvider([
            'query' => Noticias::find(),
        ]);
        
        return $this->render('index', [
            'dataProvider1' => $articulos,
            'dataProvider2' => $noticias,
            
        ]);

    }
    
    public function actionArticulos()
    {
        $dataprovider = new ActiveDataProvider([
            'query' => Articulos::find(),
        ]);

        return $this->render('articulos', [
            'dataProvider' => $dataprovider,
        ]);
        
    }
    
    public function actionNoticias()
    {
    $dataprovider = new ActiveDataProvider([
            'query' => Noticias::find(),
        ]);

        return $this->render('noticias', [
            'dataProvider' => $dataprovider,
        ]);
        
    }
    
   
    

    
}
